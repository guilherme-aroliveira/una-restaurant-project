package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        try {
            //Inherit the resouce's application
            Parent parent = FXMLLoader.load(getClass().getResource("/gui/AppInterface.fxml"));
            Scene scene = new Scene(parent);


            stage.setTitle("Restaurant");
            stage.setResizable(false);
            //scene.setFill(Color.TRANSPARENT);
            stage.setScene(scene);
            stage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Run the program
    public static void main(String[] args) {
        launch(args);
    }
}
